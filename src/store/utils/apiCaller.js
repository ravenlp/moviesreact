export default function apiCaller(url, method, path, data){
	return fetch(url + path, {
		method,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json"
		},
		body: data ? JSON.stringify(data) : null
	}).then(res => res.json());
}