import {takeLatest, call, put} from 'redux-saga/effects';

import apiCaller from '../utils/apiCaller';
import {FETCH_MOVIELIST_REQUEST, SEARCH_MOVIE_REQUEST} from './types';

import {storeMovieList, storeSearchResults} from './actions';

const TMDBApiBase = 'https://api.themoviedb.org/3';
const apiKey = '8fa68d8b01c895e8d2125ae5fd89995d';

function* fetchDiscover() {
  try {
    const res = yield call(
      apiCaller,
      TMDBApiBase,
      'GET',
      `/discover/movie?api_key=${apiKey}`
    );
    yield put(storeMovieList(res.results));
  } catch(err) {
    console.error(err);
  }
}


function* searchMovies(action) {
  const {payload: term} = action;
  try {
    const res = yield call(
      apiCaller,
      TMDBApiBase,
      'GET',
      `/search/movie?api_key=${apiKey}&query=${term}`
    );
    yield put(storeSearchResults(res.results));
  } catch(err) {
    console.error(err);
  }
}

export default [
  takeLatest(FETCH_MOVIELIST_REQUEST, fetchDiscover),
  takeLatest(SEARCH_MOVIE_REQUEST, searchMovies)
];
