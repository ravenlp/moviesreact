import {
  FETCH_MOVIELIST_REQUEST,
  FETCH_MOVIELIST_SUCCESS,

  SEARCH_MOVIE_REQUEST,
  SEARCH_MOVIE_SUCCESS,
  SEARCH_MOVIE_RESET
} from './types';

// Discover related actions
export const fetchMovieList = () => ({
  type: FETCH_MOVIELIST_REQUEST,
});

export const storeMovieList = list => ({
  type: FETCH_MOVIELIST_SUCCESS,
  payload: list,
});


// Search related actions
export const searchMovies = (term) => ({
  type: SEARCH_MOVIE_REQUEST,
  payload: term
});

export const searchMoviesReset = () => ({
  type: SEARCH_MOVIE_RESET
});

export const storeSearchResults = list => ({
  type: SEARCH_MOVIE_SUCCESS,
  payload: list,
});
