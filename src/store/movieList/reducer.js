import {
  FETCH_MOVIELIST_REQUEST,
  FETCH_MOVIELIST_SUCCESS,
  SEARCH_MOVIE_REQUEST,
  SEARCH_MOVIE_SUCCESS,
  SEARCH_MOVIE_RESET
} from './types';

const initialState = {
  list: [],
  loading: false,
  searchTerm: '',
  searchResults: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOVIELIST_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_MOVIELIST_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload,
      };
    case SEARCH_MOVIE_REQUEST:
      return {
        ...state,
        loading: true,
        searchTerm: action.payload,
        searchResults: []
      };
    case SEARCH_MOVIE_SUCCESS:
      return {
        ...state,
        loading: false,
        searchResults: action.payload,
      };
    case SEARCH_MOVIE_RESET:
      return {
        ...state,
        loading: false,
        searchResults: [],
        searchTerm: ''
      };
    default:
      return {
        ...state,
      };
  }
};

export default reducer;
