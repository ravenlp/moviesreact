export const getLoadingState = (state) => state.movies.loading;

const filterByRating = (list, rating) => {
  const filtered = list.filter(m => {
    return m.vote_average > (rating -2) && m.vote_average <= rating;
  })
  return filtered;
}

export const getMovieList = (state, filter) => {
  const list = state.movies.list;
  if(filter && filter.rating > 0) {
    return filterByRating(list, filter.rating);
  }
  return list;
}

export const getSearchedTerm = (state) => state.movies.searchTerm;

export const getMovieSearchResults = (state, filter) => {
  const list = state.movies.searchResults;
  if(filter && filter.rating > 0) {
    return filterByRating(list, filter.rating);
  }
  return list;
}