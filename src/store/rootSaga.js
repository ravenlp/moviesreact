import { all, fork} from 'redux-saga/effects';
import movieListSagas from './movieList/saga';


export default function* rootSaga() {
  yield all([
    ...movieListSagas
  ]);
}
