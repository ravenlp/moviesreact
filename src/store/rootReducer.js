import {combineReducers} from 'redux';
import movies from './movieList/reducer'

export default combineReducers({
  movies
})