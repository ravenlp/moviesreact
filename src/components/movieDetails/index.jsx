import React, {useEffect} from 'react';

import './styles.scss';

const MovieDetails = ({movie, dismiss}) => {

  const hasPoster = movie.poster_path? movie.poster_path : '';

  const details = {
    original_title: "Original Title",
    overview: "Overview",
    release_date: "Release Date",
    vote_average: "Vote Average"
  }

  if(!movie || !movie.title) {
    return null;
  }
  return (
    <div className="movieDetailsContainer">
      <div className="overlay" onClick={() => dismiss()}></div>
      <div className="movieDetails">
        <button className="close" onClick={() => dismiss()}>&lt;Back</button>
        <h3>{movie.title}</h3>
        <div className="card">
          {hasPoster && 
            <img src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`} />
          }
          {!hasPoster && <div className="noPoster">&nbsp;</div>}
        
        
          <div className="info">
            <ul>
              {Object.keys(details).map(k => (
                <li><h4>{details[k]}</h4><p>{movie[k]}</p></li>
              ))}
            </ul>
            
          </div>
        </div>
        

      </div>
    
      
    </div>
  )
}

export default MovieDetails;