import React, {useState, useEffect} from 'react';
import './styles.scss';

const SearchForm = ({term, onSubmit}) => {

  const [value, setValue] = useState(term);

  useEffect(() => {
    setValue(term);
  }, [term]);
  
  const handleChange = (e) => {
    e.preventDefault();
    setValue(e.target.value);
  }

  const handleSubmit = e => {
    e.preventDefault();
    onSubmit(value);
  }
  return (
    <div className="searchForm">
      <form onSubmit={handleSubmit}>
        <input type="text" value={value} onChange={handleChange} placeholder="Search" />
        <button type="submit" disabled={!value.length}>Search</button>
      </form>
    </div>
  )
}

export default SearchForm;