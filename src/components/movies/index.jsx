import React from 'react';

import './styles.scss';

const Movie = ({movie, onClick}) => {
  const hasPoster = movie.poster_path? movie.poster_path : ''
  return (
    <div className="movieItem" onClick={onClick}>
      
      <div className="titleContainer"><h3>{movie.title}</h3></div>
      {hasPoster && 
        <img src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`} />
      }
      {!hasPoster && <div className="noPoster">&nbsp;</div>}
    </div>
  )
}

export default Movie;