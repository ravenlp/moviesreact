import React, {useState} from 'react';
import starEmpty from '../../img/rating/star-empty.png';
import starFilled from '../../img/rating/star-filled.png';

import './styles.scss';

const Rating = ({onChange}) => {
  const [current, setCurrent] = useState(0);
  const range = [2,4,6,8,10];

  const handleChange = (rating) => {
    if(rating === current) {
      setCurrent(0);
      onChange(0);
    } else {
      setCurrent(rating);
      onChange(rating);
    }
  }
  return (
    <div className="rating">
    {range.map(r => (
      <img src={current < r? starEmpty: starFilled} key={r} onClick={() => handleChange(r)}/>
    ))}
    </div>
  )
}

export default Rating;