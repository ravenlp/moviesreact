import React, {useEffect, useCallback, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import './App.scss';

import { fetchMovieList, searchMovies, searchMoviesReset} from './store/movieList/actions';
import { getMovieList, getMovieSearchResults, getLoadingState, getSearchedTerm} from './store/movieList/selectors';

import Movie from './components/movies/';
import MovieDetails from './components/movieDetails/';
import SearchForm from './components/searchForm';
import Loader from './components/loader';
import Rating from './components/rating';

const App = () => {

  const [filter, setFilter] = useState({rating: 0});
  const handleRatingFilterChange = rating => setFilter({rating});

  const dispatch = useDispatch();
  const dispatchToProps = {
    fetchMovieListCall: useCallback(() => dispatch(fetchMovieList()), [dispatch]),
    searchMovieCall: useCallback(term => dispatch(searchMovies(term)), [dispatch]),
    searchMovieResetCall: useCallback(() => dispatch(searchMoviesReset()), [dispatch]),
  }

  const searchMovie = (term) => {
    if(term && term.length) {
      dispatchToProps.searchMovieCall(term);
    } else {
      dispatchToProps.searchMovieResetCall();
    }
  }

  const [selectedMovie, setSelectedMovie] = useState({});

  // Fetch discover content on mounting
  useEffect(() => {
    dispatchToProps.fetchMovieListCall();
  }, []);

  let movies = [];
  movies = useSelector(state => getMovieList(state, filter));
  const searchResults = useSelector(state => getMovieSearchResults(state, filter));
  const loadingState = useSelector(getLoadingState);
  const searchingTerm = useSelector(getSearchedTerm);
  
  let moviesToShow = movies;
  let title = 'Discover movies';
  if(searchingTerm && searchingTerm.length){
    moviesToShow = searchResults;
    title = 'Search Results'
  }

  return (
    <div className="App">
      <header>
        {loadingState && <Loader />}
        <h1>Movie Time!</h1>
        <SearchForm term={searchingTerm} onSubmit={searchMovie} />
      </header>
      <div className="toolbar">
        {searchingTerm && <button onClick={dispatchToProps.searchMovieResetCall}>&lt;Back</button>}
        <h3 className="title">{title}</h3>
        <Rating onChange={handleRatingFilterChange} />
      </div>
      <div className="movieListContainer">
        <div className="movieList">
          {moviesToShow.map(m => (
            <Movie movie={m} key={m.id} onClick={() => setSelectedMovie(m)}/>
          ))}
        </div>
      </div>
      <MovieDetails movie={selectedMovie} dismiss={() => setSelectedMovie({})}/>
      
    </div>
  );
}

export default App;
